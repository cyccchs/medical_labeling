﻿#include <cstdio>
#include <opencv2/opencv.hpp>
//C:\Users\IRIS\source\repos\opencv\opencv\Source.cpp
using namespace cv;
using namespace std;

void onMouse(int Event, int x, int y, int flags, void* param);
int file_exist(char const*);
Point VertexLeftTop(-1, -1);
Point VertexRightDown(-1, -1);
Point center;
Point mouse;
int lenth, width, mode = 1;
int key = -1;
char ori[200];
char path[200];
int point_size = 20;
float alpha = 0.5;
int i_result = 0;
Mat src, src2, draw, draw2, src_tmp, _src, _draw;

int main() {
	namedWindow("image", WINDOW_NORMAL);
	namedWindow("draw", WINDOW_NORMAL);
	setMouseCallback("image", onMouse, NULL);
	int flag = 0;

	for (int i = 1; i <=3021; i++)
	{
		snprintf(ori, sizeof(ori), "../../../img/20200605_073023_1.mp4/(%d).jpg", i);
		snprintf(path, sizeof(path), "../../../img/20200605_073023_1.mp4/result/%d.jpg", i);
		try{
			file_exist(ori);
		}catch(char const* err_message){
			cout << err_message << endl;
			key = 113;
			break;
		}
		
		cout << ori << endl;
		src = imread(ori, IMREAD_COLOR);

		src.copyTo(src2); 

		src.copyTo(_src);
		src.copyTo(src_tmp);


		addWeighted(src, 1, src, -1, 1, draw);

		draw.copyTo(draw2);
		draw.copyTo(_draw);

		width = 0;
		lenth = 0;

		while (true)
		{
			if (key == 83) //right
			{
				key = -1;
				break;
			}
			if (key == 81 && i > 1) //left
			{
				key = -1;
				i-=2;
				break;
			}
			if (key == 9) //tab
			{
				if (mode == 1)	mode = 2;
				else mode = 1;
			}

			if (key == 32 && flag == 1)  //save
			{
				imwrite(path, draw);
				cout << "picture " << path << " saved!" << endl;
				flag = 0;
				break;
			}

			if (key == 27)      //ESC
			{
				src2.copyTo(src);
				src2.copyTo(src_tmp);
				draw2.copyTo(draw);
			}

			if (mode == 1) {
				if (key == 13) //Enter
				{
					ellipse(draw, center, Size(width, lenth), 0, 0, 360, Scalar(255, 255, 255), cv::FILLED);
					imshow("draw", draw);
					flag = 1;
				}

				if (VertexRightDown.x != -1 && VertexRightDown.y != -1)
				{
					//cout << "(" << VertexLeftTop.x << "," << VertexLeftTop.y << ")" << endl;
					rectangle(src, Rect(VertexLeftTop, VertexRightDown), Scalar(255, 0, 0), 2);
					width = abs(VertexRightDown.x - VertexLeftTop.x) / 2;
					lenth = abs(VertexRightDown.y - VertexLeftTop.y) / 2;
					center.x = VertexRightDown.x + (VertexLeftTop.x - VertexRightDown.x) / 2;
					center.y = VertexRightDown.y + (VertexLeftTop.y - VertexRightDown.y) / 2;
					circle(src, center, 1, Scalar(255, 255, 0), 2);
					ellipse(src, center, Size(width, lenth), 0, 0, 360, Scalar(255, 255, 255), 1);
					VertexRightDown.x = -1;
					VertexRightDown.y = -1;

				}
			}
			if (mode == 2) {
				if (key == 46) //>>
				{
					point_size++;
					src_tmp.copyTo(src);
					circle(src, mouse, point_size, Scalar(255, 255, 255), 1);
				}
				if (key == 44) //<<
				{
					if (point_size <= 0) point_size = 0;
					else point_size--;
					src_tmp.copyTo(src);
					circle(src, mouse, point_size, Scalar(255, 255, 255), 1);
				}
				if (key == 13) //Enter
				{
					flag = 1;
				}
				if (key == 26)      //ctrl+ z
				{
					_src.copyTo(src);
					_src.copyTo(src_tmp);
					_draw.copyTo(draw);

				}
			}
			imshow("image", src);
			imshow("draw", draw);
			key = waitKey(1);
			if (key != -1) cout << "key :" << key << endl;
			if(key == 113){			//q
				break;
			}

		}
		if(key == 113){				//q
			break;
		}

	}
	destroyAllWindows();
	return 0;
}


void onMouse(int Event, int x, int y, int flags, void* param) {
	if (Event == EVENT_LBUTTONDOWN) {
		VertexLeftTop.x = x;
		VertexLeftTop.y = y;
		if (mode == 2) {
			src_tmp.copyTo(_src);
			draw.copyTo(_draw);
		}
	}
	if (flags == 33)
	{
		VertexRightDown.x = x;
		VertexRightDown.y = y;
		if (mode == 1) src2.copyTo(src);

		if (mode == 2) {
			circle(src, VertexRightDown, point_size, Scalar(255, 255, 255), cv::FILLED);
			circle(draw, VertexRightDown, point_size, Scalar(255, 255, 255), cv::FILLED);
			src.copyTo(src_tmp);
		}

	}

	if (Event == EVENT_LBUTTONUP)
	{
		VertexRightDown.x = x;
		VertexRightDown.y = y;


	}
	if (Event == EVENT_MOUSEMOVE)
	{
		mouse.x = x;
		mouse.y = y;
		if (mode == 2) {
			src_tmp.copyTo(src);
			//cout << "(" << mouse.x << "," << mouse.y << ")" << endl;
			circle(src, mouse, point_size, Scalar(255, 255, 255), 1);
		}
	}

}
int file_exist(char const* _path){
	if (FILE *file = fopen(_path, "r")) {
		fclose(file);
		return true ;
	} else {
		throw "\nERR: The given path does not exist\n";
		return false;
	}  
}
